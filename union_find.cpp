#include "union_find.h"

template <typename T>
Union_find<T>::Union_find(int num) : size(num) {
    this->ver_shortcut_array = new ver*[num];
    for (int i=0; i<num; i++){
        this->ver_shortcut_array[i] = new ver(i, new T, nullptr, 1);
    }
}

template <typename T>
Union_find<T>::~Union_find() {
    for (int i=0; i<this->size; i++) {
        delete this->ver_shortcut_array[i]->data;
        delete this->ver_shortcut_array[i];
    }
    delete[] this->ver_shortcut_array;
}

template <typename T>
int Union_find<T>::find(int k) {
    if (k >= this->size || k < 0)
        return -1;
    ver* k_root = this->ver_shortcut_array[k];
    while (k_root->next != nullptr)
        k_root = k_root->next;
    ver* temp_ver = this->ver_shortcut_array[k];
    ver* update_ver;
    if (temp_ver != k_root) {
        while (temp_ver->next != k_root) {
            update_ver = temp_ver;
            temp_ver = temp_ver->next;
            update_ver->next = k_root;
        }
    }
    return k_root->num;
}

template <typename T>
int Union_find<T>::do_union(int p, int q) {
    if (p==q || p >= this->size || p < 0 || q >= this->size || q < 0)
        return -1;
    ver* p_root = this->ver_shortcut_array[p];
    ver* q_root = this->ver_shortcut_array[q];
    while (p_root->next != nullptr)
        p_root = p_root->next;
    while (q_root->next != nullptr)
        q_root = q_root->next;
    if (p_root == q_root)
        return p_root->num;
    if (p_root->group_size >= q_root->group_size) {
        q_root->next = p_root;
        p_root->group_size = p_root->group_size + q_root->group_size;
        return p_root->num;
    }
    else {
        p_root->next = q_root;
        q_root->group_size = q_root->group_size + p_root->group_size;
        return q_root->num;
    }

}

template <typename T>
T* Union_find<T>::getter(int key) {
    if (key >= this->size || key < 0)
        return nullptr;
    ver* root = this->ver_shortcut_array[key];
    while (root->next != nullptr)
        root = root->next;
    return root->data;
}

template <typename T>
T* Union_find<T>::getter_self(int key) {
    if (key >= this->size || key < 0)
        return nullptr;
    return this->ver_shortcut_array[key]->data;
}

template <typename T>
bool Union_find<T>::is_root(int k) {
    return ((0<=k && k<size) && this->ver_shortcut_array[k]->next == nullptr);
}

#ifndef DRY2_UNION_FIND_H
#define DRY2_UNION_FIND_H
/*
 * This union-find uses route-shortening (called upon while using find)
 * and size comparison decided union (implemented in do_union).
 * The number of vertices is set on build time.
 *
 * This structure also supports holding specific data (through template pointers) in each vertex. This does NOT
 * imply automated union of the given data. If you wish to perform union of the data, do so manually on the data
 * returned by getter(ver_num) for the sub-group defined by ver_num.
 * Pay notice to the difference between "getter" and "getter_self".
 *
 * Size is set upon construction, so attention to space constraints is recommended.
 */
template <typename T>
class Union_find {
public:
    int find(int); //O(1) , returns the num of the group the object found belongs to (aka the root's number)
    int do_union(int, int); //O(1) , unites the two groups and returns the num of the new group
    Union_find(int); //O(n) , This structure's size is determined on build time by this int arg (fixed size).
    ~Union_find(); //O(n) , Important! This also deletes the data for each ver. If this is unwanted make the necessary changes in the cpp file.
    T* getter(int); //returns the root's data address. Use this to insert data into the structure and to extract it.
    T* getter_self(int); //returns the self's data
    bool is_root(int); //returns false for the case the given key isn't a root or it is out of bounds (<0 or size<=)

private:
    //Implementation details. Please ignore.
    struct ver {
        int num; //range [0 , size-1]
        T* data;
        ver* next; //when next == nullptr, ver is the root
        int group_size; //range [1 , size]
        ver(int num, T* data, ver* next = nullptr, int group_size = 1) : num(num) , data(data) , next(next),
                                                                         group_size(group_size) {}
    };
    const int size;
    ver** ver_shortcut_array;
};

#endif //DRY2_UNION_FIND_H
